/******************************************************************************
* File Name: main.c
*
* Version 1.0
*
* Description: 
*  This program read Color Sensors through the I2C interface and send data  
*  results to the UART. 
*
* Related Document: 
*  Schematic.pdf
*
* Hardware Dependency: 
*  See Diagram_Block.pdf
*
* History:
*	01/10/2020 - First Release - Emerson Nery
*
*******************************************************************************/

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules


#define VEML3328_SLAVE_ADD 0x10		// Slave Address for VEML3328
#define CONF 0x00
#define C_DATA 0x04					// Clear Filter Option
#define R_DATA 0x05					// Red Filter Option
#define G_DATA 0x06					// Green Filter Option
#define B_DATA 0x07					// Blue Filter Option
#define IR_DATA 0x08				// InfraRed Filter Option

#define BUFFER_SIZE 0x02			// I2C Buffer Size

union dataColor {					// Union Data to get bytes and Integer Values of Color Sensor
   unsigned int i;					// integer value
   unsigned char b[BUFFER_SIZE];	// byte value to send to I2C
} txColor, rxColor, configColor; 	//command to send, command to receive and command to config the sensor


//-----------------------------------------------------------------------------
// FUNCTION NAME: read_VEML3328
//
// DESCRIPTION:	Function to get values from the Color Sensor. Just set the color channel.
//-----------------------------------------------------------------------------
unsigned int read_VEML3328(unsigned char channel)
{
	txColor.b[0] = channel;		// Clear, Red, Blue, Green or InfraRed
	txColor.b[1] = 0x00;		
	rxColor.b[0] = 0x00;		// 0x0000 to get data
	rxColor.b[1] = 0x00;
	
	I2CHW_1_bWriteBytes(VEML3328_SLAVE_ADD, txColor.b, BUFFER_SIZE, I2CHW_1_CompleteXfer);	// Send Command to Setup
    while(!(I2CHW_1_bReadI2CStatus() & I2CHW_WR_COMPLETE));									// Wait for Complete transmission
    I2CHW_1_ClrWrStatus();																	// Clear Status Interrupt
    
	I2CHW_1_fReadBytes(VEML3328_SLAVE_ADD, rxColor.b, BUFFER_SIZE, I2CHW_1_CompleteXfer);	// Send Command to Receive Data
    while(!(I2CHW_1_bReadI2CStatus() & I2CHW_RD_COMPLETE));									// Wait for Complete transmission
    I2CHW_1_ClrRdStatus();																	// Clear Status Interrupt
	
	return rxColor.i;																		// return an integer 16-bit
} 

//----------------------------------------------------------------------------
// FUNCTION NAME: main
//
// DESCRIPTION: Read all color filters and them send values to the UART
//----------------------------------------------------------------------------

void main(void)
{
	char * strPtr;                        // Parameter pointer
	unsigned int VEML3328_C, VEML3328_R, VEML3328_G, VEML3328_B, VEML3328_IR; // values for each color on RAM
	
	LED_GREEN_Start();						// Enable LED Green Control
	LED_RED_Start();						// Enable LED Red Control
	LED_BLUE_Start();						// Enable LED Blue Control
	LED_RESET_Start();						// Enable Reset LED Control
	
	SENSOR_CLK_Start();						// Enable CLK Sensor
	SENSOR_RESET_Start();					// Enable Reset Sensor Control
	
	UART_1_CmdReset();                      // Initialize receiver/cmd
    UART_1_IntCntl(UART_1_ENABLE_RX_INT);     // Enable RX interrupts
	UART_1_Start(UART_1_PARITY_NONE);         // Enable UART
    
    I2CHW_1_Start();						// Initialize I2C
    I2CHW_1_EnableMstr();					// Enable I2C Master Comm
    
	M8C_EnableGInt;							// Enable General Interrupt
    
	I2CHW_1_EnableInt(); 					// Enable Interrupt from I2C Sensors
    
	UART_1_CPutString("\r\nData Teste with Color Sensors \r\n");	// Message to teste UART Communication	

	LED_GREEN_Invert();						// Turn ON LED GReen
		
	configColor.b[0] = 0x11;																	// Low Byte to Shutdown Command
	configColor.b[1] = 0x80;																	// High Byte to Shutdown Command
	I2CHW_1_bWriteBytes(VEML3328_SLAVE_ADD, configColor.b, BUFFER_SIZE, I2CHW_1_CompleteXfer); 	// Shut Down Color Sensor
    while(!(I2CHW_1_bReadI2CStatus() & I2CHW_WR_COMPLETE));										// Wait for Complete transmission
    I2CHW_1_ClrWrStatus();																		// Clear Status Interrupt
	
	configColor.b[0] = 0x10;																	// Low Byte to Enable Sensor Command
	configColor.b[1] = 0x00;																	// High Byte to Enable Sensor Command
	I2CHW_1_bWriteBytes(VEML3328_SLAVE_ADD, configColor.b, BUFFER_SIZE, I2CHW_1_CompleteXfer);	// Enable Color Sensor 
    while(!(I2CHW_1_bReadI2CStatus() & I2CHW_WR_COMPLETE));										// Wait for Complete transmission
    I2CHW_1_ClrWrStatus();																		// Clear Status Interrupt
		
   	while(1)
	{
      	VEML3328_C = read_VEML3328(C_DATA);														// get color value with No Filters
		VEML3328_B = read_VEML3328(B_DATA);														// get color value with Blue Filter
        VEML3328_G = read_VEML3328(G_DATA);														// get color value with Green Filter
		VEML3328_R = read_VEML3328(R_DATA);														// get color value with Red Filter
		VEML3328_IR = read_VEML3328(IR_DATA);													// get color value with Infra Red Filter
		
		if(UART_1_bCmdCheck()) 
		{                    																	// Wait for command  
			if(strPtr = UART_1_szGetParam()) 
			{       																			// More than delimiter
            	UART_1_CPutString("Found valid command\r\nCommand =>");  
   				UART_1_PutString(strPtr);             											// Print out command 
   				UART_1_CPutString("<\r\nParamaters:\r\n");  
   				while(strPtr = UART_1_szGetParam())
				{ 																				// loop on each parameter  
      				UART_1_CPutString("   <");  
      				UART_1_PutString(strPtr);          											// Print each parameter  
      				UART_1_CPutString(">\r\n");  
   				} 
			} 
			UART_1_CmdReset();                         											// Reset command buffer  
		} 
  
   }

}
