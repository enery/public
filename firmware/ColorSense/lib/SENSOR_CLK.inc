;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME:   SENSOR_CLK.inc
;;  Version: 2.00, Updated on 2015/3/4 at 22:26:37
;;  Generated by PSoC Designer 5.4.3191
;;
;;  DESCRIPTION:  Assembler declarations for the LED user module interface.
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2015. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************

include "m8c.inc"

SENSOR_CLK_Port:      equ   0x0


SENSOR_CLK_PortDR:    equ   PRT0DR

SENSOR_CLK_PinMask:   equ  0x80

SENSOR_CLK_Drive:     equ   1

SENSOR_CLK_ON:        equ   1
SENSOR_CLK_OFF:       equ   0

;------------------------------------------------------
;  Register Address Constants for  SENSOR_CLK
;------------------------------------------------------

;------------------------------------------------------
;  Macros for  SENSOR_CLK
;------------------------------------------------------





; end of file SENSOR_CLK.inc
