;  Generated by PSoC Designer 5.4.3191
;
; LED_GREENPin address and mask equates
LED_GREENPin_Data_ADDR:	equ	0h
LED_GREENPin_DriveMode_0_ADDR:	equ	100h
LED_GREENPin_DriveMode_1_ADDR:	equ	101h
LED_GREENPin_DriveMode_2_ADDR:	equ	3h
LED_GREENPin_GlobalSelect_ADDR:	equ	2h
LED_GREENPin_IntCtrl_0_ADDR:	equ	102h
LED_GREENPin_IntCtrl_1_ADDR:	equ	103h
LED_GREENPin_IntEn_ADDR:	equ	1h
LED_GREENPin_MASK:	equ	1h
; LED_GREENPin_Data access macros
;   GetLED_GREENPin_Data macro, return in a
macro GetLED_GREENPin_Data
	mov		a,[Port_0_Data_SHADE]
	and		a, 1h
endm
;   SetLED_GREENPin_Data macro
macro SetLED_GREENPin_Data
	or		[Port_0_Data_SHADE], 1h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[LED_GREENPin_Data_ADDR], a
endm
;   ClearLED_GREENPin_Data macro
macro ClearLED_GREENPin_Data
	and		[Port_0_Data_SHADE], ~1h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[LED_GREENPin_Data_ADDR], a
endm

; LED_BLUEPin address and mask equates
LED_BLUEPin_Data_ADDR:	equ	0h
LED_BLUEPin_DriveMode_0_ADDR:	equ	100h
LED_BLUEPin_DriveMode_1_ADDR:	equ	101h
LED_BLUEPin_DriveMode_2_ADDR:	equ	3h
LED_BLUEPin_GlobalSelect_ADDR:	equ	2h
LED_BLUEPin_IntCtrl_0_ADDR:	equ	102h
LED_BLUEPin_IntCtrl_1_ADDR:	equ	103h
LED_BLUEPin_IntEn_ADDR:	equ	1h
LED_BLUEPin_MASK:	equ	4h
; LED_BLUEPin_Data access macros
;   GetLED_BLUEPin_Data macro, return in a
macro GetLED_BLUEPin_Data
	mov		a,[Port_0_Data_SHADE]
	and		a, 4h
endm
;   SetLED_BLUEPin_Data macro
macro SetLED_BLUEPin_Data
	or		[Port_0_Data_SHADE], 4h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[LED_BLUEPin_Data_ADDR], a
endm
;   ClearLED_BLUEPin_Data macro
macro ClearLED_BLUEPin_Data
	and		[Port_0_Data_SHADE], ~4h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[LED_BLUEPin_Data_ADDR], a
endm

; LED_REDPin address and mask equates
LED_REDPin_Data_ADDR:	equ	0h
LED_REDPin_DriveMode_0_ADDR:	equ	100h
LED_REDPin_DriveMode_1_ADDR:	equ	101h
LED_REDPin_DriveMode_2_ADDR:	equ	3h
LED_REDPin_GlobalSelect_ADDR:	equ	2h
LED_REDPin_IntCtrl_0_ADDR:	equ	102h
LED_REDPin_IntCtrl_1_ADDR:	equ	103h
LED_REDPin_IntEn_ADDR:	equ	1h
LED_REDPin_MASK:	equ	10h
; LED_REDPin_Data access macros
;   GetLED_REDPin_Data macro, return in a
macro GetLED_REDPin_Data
	mov		a,[Port_0_Data_SHADE]
	and		a, 10h
endm
;   SetLED_REDPin_Data macro
macro SetLED_REDPin_Data
	or		[Port_0_Data_SHADE], 10h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[LED_REDPin_Data_ADDR], a
endm
;   ClearLED_REDPin_Data macro
macro ClearLED_REDPin_Data
	and		[Port_0_Data_SHADE], ~10h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[LED_REDPin_Data_ADDR], a
endm

; SENSOR_RESETPin address and mask equates
SENSOR_RESETPin_Data_ADDR:	equ	0h
SENSOR_RESETPin_DriveMode_0_ADDR:	equ	100h
SENSOR_RESETPin_DriveMode_1_ADDR:	equ	101h
SENSOR_RESETPin_DriveMode_2_ADDR:	equ	3h
SENSOR_RESETPin_GlobalSelect_ADDR:	equ	2h
SENSOR_RESETPin_IntCtrl_0_ADDR:	equ	102h
SENSOR_RESETPin_IntCtrl_1_ADDR:	equ	103h
SENSOR_RESETPin_IntEn_ADDR:	equ	1h
SENSOR_RESETPin_MASK:	equ	20h
; SENSOR_RESETPin_Data access macros
;   GetSENSOR_RESETPin_Data macro, return in a
macro GetSENSOR_RESETPin_Data
	mov		a,[Port_0_Data_SHADE]
	and		a, 20h
endm
;   SetSENSOR_RESETPin_Data macro
macro SetSENSOR_RESETPin_Data
	or		[Port_0_Data_SHADE], 20h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[SENSOR_RESETPin_Data_ADDR], a
endm
;   ClearSENSOR_RESETPin_Data macro
macro ClearSENSOR_RESETPin_Data
	and		[Port_0_Data_SHADE], ~20h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[SENSOR_RESETPin_Data_ADDR], a
endm

; LED_RESETPin address and mask equates
LED_RESETPin_Data_ADDR:	equ	0h
LED_RESETPin_DriveMode_0_ADDR:	equ	100h
LED_RESETPin_DriveMode_1_ADDR:	equ	101h
LED_RESETPin_DriveMode_2_ADDR:	equ	3h
LED_RESETPin_GlobalSelect_ADDR:	equ	2h
LED_RESETPin_IntCtrl_0_ADDR:	equ	102h
LED_RESETPin_IntCtrl_1_ADDR:	equ	103h
LED_RESETPin_IntEn_ADDR:	equ	1h
LED_RESETPin_MASK:	equ	40h
; LED_RESETPin_Data access macros
;   GetLED_RESETPin_Data macro, return in a
macro GetLED_RESETPin_Data
	mov		a,[Port_0_Data_SHADE]
	and		a, 40h
endm
;   SetLED_RESETPin_Data macro
macro SetLED_RESETPin_Data
	or		[Port_0_Data_SHADE], 40h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[LED_RESETPin_Data_ADDR], a
endm
;   ClearLED_RESETPin_Data macro
macro ClearLED_RESETPin_Data
	and		[Port_0_Data_SHADE], ~40h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[LED_RESETPin_Data_ADDR], a
endm

; SENSOR_CLKPin address and mask equates
SENSOR_CLKPin_Data_ADDR:	equ	0h
SENSOR_CLKPin_DriveMode_0_ADDR:	equ	100h
SENSOR_CLKPin_DriveMode_1_ADDR:	equ	101h
SENSOR_CLKPin_DriveMode_2_ADDR:	equ	3h
SENSOR_CLKPin_GlobalSelect_ADDR:	equ	2h
SENSOR_CLKPin_IntCtrl_0_ADDR:	equ	102h
SENSOR_CLKPin_IntCtrl_1_ADDR:	equ	103h
SENSOR_CLKPin_IntEn_ADDR:	equ	1h
SENSOR_CLKPin_MASK:	equ	80h
; SENSOR_CLKPin_Data access macros
;   GetSENSOR_CLKPin_Data macro, return in a
macro GetSENSOR_CLKPin_Data
	mov		a,[Port_0_Data_SHADE]
	and		a, 80h
endm
;   SetSENSOR_CLKPin_Data macro
macro SetSENSOR_CLKPin_Data
	or		[Port_0_Data_SHADE], 80h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[SENSOR_CLKPin_Data_ADDR], a
endm
;   ClearSENSOR_CLKPin_Data macro
macro ClearSENSOR_CLKPin_Data
	and		[Port_0_Data_SHADE], ~80h
	mov		a, [Port_0_Data_SHADE]
	mov		reg[SENSOR_CLKPin_Data_ADDR], a
endm

; RXuart address and mask equates
RXuart_Data_ADDR:	equ	4h
RXuart_DriveMode_0_ADDR:	equ	104h
RXuart_DriveMode_1_ADDR:	equ	105h
RXuart_DriveMode_2_ADDR:	equ	7h
RXuart_GlobalSelect_ADDR:	equ	6h
RXuart_IntCtrl_0_ADDR:	equ	106h
RXuart_IntCtrl_1_ADDR:	equ	107h
RXuart_IntEn_ADDR:	equ	5h
RXuart_MASK:	equ	1h
; TXuart address and mask equates
TXuart_Data_ADDR:	equ	4h
TXuart_DriveMode_0_ADDR:	equ	104h
TXuart_DriveMode_1_ADDR:	equ	105h
TXuart_DriveMode_2_ADDR:	equ	7h
TXuart_GlobalSelect_ADDR:	equ	6h
TXuart_IntCtrl_0_ADDR:	equ	106h
TXuart_IntCtrl_1_ADDR:	equ	107h
TXuart_IntEn_ADDR:	equ	5h
TXuart_MASK:	equ	2h
; I2CHW_1SDA address and mask equates
I2CHW_1SDA_Data_ADDR:	equ	4h
I2CHW_1SDA_DriveMode_0_ADDR:	equ	104h
I2CHW_1SDA_DriveMode_1_ADDR:	equ	105h
I2CHW_1SDA_DriveMode_2_ADDR:	equ	7h
I2CHW_1SDA_GlobalSelect_ADDR:	equ	6h
I2CHW_1SDA_IntCtrl_0_ADDR:	equ	106h
I2CHW_1SDA_IntCtrl_1_ADDR:	equ	107h
I2CHW_1SDA_IntEn_ADDR:	equ	5h
I2CHW_1SDA_MASK:	equ	20h
; I2CHW_1SCL address and mask equates
I2CHW_1SCL_Data_ADDR:	equ	4h
I2CHW_1SCL_DriveMode_0_ADDR:	equ	104h
I2CHW_1SCL_DriveMode_1_ADDR:	equ	105h
I2CHW_1SCL_DriveMode_2_ADDR:	equ	7h
I2CHW_1SCL_GlobalSelect_ADDR:	equ	6h
I2CHW_1SCL_IntCtrl_0_ADDR:	equ	106h
I2CHW_1SCL_IntCtrl_1_ADDR:	equ	107h
I2CHW_1SCL_IntEn_ADDR:	equ	5h
I2CHW_1SCL_MASK:	equ	80h
